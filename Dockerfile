# Étape de construction
FROM node:20 AS build

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers de configuration Angular
COPY angular.json ./
COPY package.json ./
COPY tsconfig.json ./

# Installer les dépendances
RUN npm install

# Copier les fichiers source de l'application dans le répertoire de travail
COPY . .

# Construire l'application Angular
RUN npm run build

# Étape de production
FROM nginx:alpine

# Copier les fichiers de construction de l'étape de construction dans le répertoire NGINX
COPY --from=build /app/dist /usr/share/nginx/html

# Exposer le port 80 pour accéder à l'application via le navigateur
EXPOSE 80

# Commande pour démarrer NGINX une fois le conteneur lancé
CMD ["nginx", "-g", "daemon off;"]
